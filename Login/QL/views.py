from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .forms import LoginForm



def home(request):
    # return render(request, 'components/home.html')
    return redirect('index/')


def data(request):
    return render(request, 'components/data_table.html')

def login22(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            pass
            # username = form.cleaned_data.get("username")
            # password = form.cleaned_data.get("password")
    return render(request, 'components/login.html')


def signin(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(
            request,
            username=username,
            password=password
        )
        if user is None:
            return HttpResponse("Đăng nhập không thành công")
        login(request, user)
        return redirect('/index/')
    else:
        return render(request, 'components/login.html')

#
# @login_required
# def index(request):
#     return render(request, 'components/index.html', {})
def index(request):
    # return redirect('index/')
    return render(request, 'components/index.html')


def logout_view(request):
    logout(request)
    # return render(request, 'components/logout.html')
    return render(request, 'components/logout.html')
