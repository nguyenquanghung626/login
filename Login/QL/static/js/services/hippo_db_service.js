// HippoDbServices class: Singleton class that provides access to the HippoDB
// database.
import { API_ADDRESS } from '../config/API_ADDRESS.js';
import { fetchData } from '../controllers/fetch_controller.js';


var HippoDbServices = (function () {
    return {
        // Authentication
        getToken: function (username, password) {
            let url = `${API_ADDRESS}/api/token`;
            let body = {
                username: username,
                password: password
            };
            return fetchData(url, 'POST', body);
        },
        getTokenFromRefresh: function (refresh_token) {
            let url = `${API_ADDRESS}/api/token/refresh`;
            let body = {
                refresh: refresh_token
            };
            return fetchData(url, 'POST', body);
        },
//        searchSubstring: function (table_name, column_name, search_string, limit, offset) {
//            // All private members are accessible here
//            var url = `${API_ADDRESS}/api/table/${table_name}/${column_name}/search_string?string=${search_string}&limit=${limit}&offset=${offset}`;
//            return fetchData(url, 'GET', null, true);
//        },
//        whereIn: function (table_name, column_name, list_id, limit, offset) {
//            // All private members are accessible here
//            var url = `${API_ADDRESS}/api/table/${table_name}/${column_name}/where_in?limit=${limit}&offset=${offset}`;
//            var body = { list: list_id };
//            return fetchData(url, 'POST', body, true);
//        },
        totalRows: function (table_name) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/select_all?limit=1`;
            return fetchData(url, 'GET', null, true);
        },
        selectAll: function (table_name, limit, offset) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/select_all?limit=${limit}&offset=${offset}`;
            return fetchData(url, 'GET', null, true);
        },
        selectWhere: function (table_name,   column_name, value) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/select_where?${column_name}=${value}`;
            return fetchData(url, 'GET', null, true);
        },
        getTableInfo: function (table_name) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/info`;
            return fetchData(url, 'GET', null, true);
        },
        insert: function (table_name, data) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/insert`;
            return fetchData(url, 'POST', data, true);
        },
        update: function (table_name, data) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table/${table_name}/update/${data['id']}`;
            return fetchData(url, 'PUT', data, true);
        },
        deleteWhere: function (table_name, column_name, value) {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/${table_name}/delete_where?${column_name}=${value}`;
            return fetchData(url, 'DELETE', null, true);
        },
        // Table management
        listAllTable: function () {
            // All private members are accessible here
            var url = `${API_ADDRESS}/api/table`;
            return fetchData(url, 'GET', null, true);
        },
        createTable: function (table_name, url) {
            // All private members are accessible here
            var apiUrl = `${API_ADDRESS}/api/table`;
            var body = { table_name: table_name, url: url };
            return fetchData(apiUrl, 'POST', body, true);
        }
    };
})();

export { HippoDbServices };