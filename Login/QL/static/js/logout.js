import { setCookie } from './utils/cookies.js'
import TokenController from "./controllers/token_controller.js";
import JWT from "./lib/jwt.js";
import AuthController from './controllers/auth_controller.js';

$(document).ready(() => {
    let btnLogout = document.querySelector('#btn-logout');
    btnLogout.addEventListener('click',AuthController.handleLogout);
})