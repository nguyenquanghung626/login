//import AutoComplete2 from './lib/autocomplete2.mjs';
//import PaginationServices from './pages/sales/pagination.mjs';
import { BaseTable } from './components/base_table.js';
//import ChipsHistoryTable from './components/chips_history_table.mjs';
//import { debounce } from './utils/debounce.mjs';
import DbDisplayServices from './services/db_display_service.js';
import PaginationLimit from './components/pagination_limit.js';
import AuthController from './controllers/auth_controller.js'

const tableNameInput = document.querySelector('.table-name-input');
const tableNameInputButton = document.querySelector('.table-name-input-button');
//const searchBarInput = document.querySelector('#search-bar__input');
//const searchBarButton = document.querySelector('#search-bar__button');

//var autoComplete2 = new AutoComplete2(searchBarInput, searchBarButton);
$(document).ready(() => {
 $('#logout_container').load('../../logout/', () => {
        let btnLogout = document.querySelector('#btn-logout');
        btnLogout.addEventListener('click', AuthController.handleLogout);
      });
});
var limit = 8;
var listItems = []; // list of record items
//var totalRowsMatched = {
//    value: 0,
//    set: function (value) {
//        if (this.value == value) return;
//        this.value = value;
//        // Update num of rows matched and update pagination
//        $('#n-related-words').text(value);
//        let nPages = Math.ceil(this.value / limit);
//        PaginationServices.updateOptions({ max_value: nPages, selected_value: 1 });
//    },
//    get: function () {
//        return this.value;
//    }
//};

// Search bar field options
//const generateSelectOptions = function (listFilteredFields) {
//    return listFilteredFields.map(field => {
//        return `<option value="${field}">${field}</option>`
//    }).join('');
//}

// Init table
let baseTable = new BaseTable();
//let chipsHistoryTable = new ChipsHistoryTable();

let tableName = "";
let listFilteredFields = [];
let filteredField = "";
tableNameInput.value = "test5";
tableNameInputButton.addEventListener('click', () => {
    handleChangeTableName(tableName = tableNameInput.value);
});
//chipsHistoryTable.render();

//PaginationServices.initPagination();
let paginationLimit = new PaginationLimit();
limit = paginationLimit.getLimit();
//$("#table-base__loading").load("components/loading.html");
//$(".modal-list-table").load("components/modal_list_table.html", () => {
//    $('body').append($('<script type="module" />').attr('src', 'js/components/modal_list_table.mjs'));
//    $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', 'css/modal_list_table.css'));
//});

// DOM Event listeners
//searchBarButton.addEventListener('click', () => handleClickSearchBarButton());
//searchBarInput.addEventListener('input', (event) => handleInputSearchBar(event));

const handleChangeFilteredField = function () {
    filteredField = this.value;
    fetchPageRecords();
}

const handleChangeTableName = async function (tableName) {
    let columnsResp = await DbDisplayServices.fetchColumns(tableName)
    if (!columnsResp || !columnsResp.data) return;
    let columns = columnsResp.data;
    let headers = columns.map(col => col.name);
//    chipsHistoryTable.insertTableName(tableName);
    listFilteredFields = headers;
//    $('#search-bar-fields__selec  t').html(generateSelectOptions(listFilteredFields));
//    $('#search-bar-fields__select').on("change", handleChangeFilteredField);
//    filteredField = listFilteredFields;
    baseTable.init(headers);

    // Fetch total rows from resp select all
    let totalRowsResp = await DbDisplayServices.fetchTotalRows(tableName)
    if (totalRowsResp.status === 'success') {
////        let total_rows = totalRowsResp['meta']['total_rows'];
////        document.querySelector('#total-rows').innerText = total_rows;
        fetchPageRecords();
    }
}
//
//const handleInputSearchBar = (event) => {
//    debounce(async () => {
//        let value = searchBarInput.value;
//        if (filteredField == "") return;
//        if (value.length == 0) {
//            autoComplete2.autocomplete([]);
//            fetchPageRecords();
//            return;
//        }
//        let response = await DbDisplayServices.fetchSearchWithField(tableName, filteredField, value, 8, 1);
//        if (value != searchBarInput.value) return;
//        // Check building model
//        if (response['status'] == 'error') {
//            alert('Error: ' + response['detail']);
//        } else {
//            let nRelatedWords = response['meta']['total_rows'];
//            totalRowsMatched.set(nRelatedWords);
//            // Set autocomplete
//            if (!response['data']) return;
//            if (response['data'].length == 0) baseTable.renderRows([]);
//            listItems = response['data'];
//            autoComplete2.autocomplete(response['data']);
//        }
//    }, 100)();
//    if (event.keyCode === 13) {
//        searchBarButton.click();
//    }
//}

//const handleClickSearchBarButton = async function (event) {
//    let value = searchBarInput.value;
//    if (filteredField == "") return;
//    if (value.length == 0 || tableNameInput.value.length == 0) return;

//    let recordsResp = await DbDisplayServices.fetchSearchWithField(tableName, filteredField, value, limit, 1);
//    if (value != searchBarInput.value) return;

//    // Check building model
//    if (recordsResp['status'] == 'error') {
//        alert('Error: ' + recordsResp['detail']);
//    } else {
//        let nRelatedWords = recordsResp['meta']['total_rows'];
//        totalRowsMatched.set(nRelatedWords);
//
//        // Set autocomplete
//        if (!recordsResp['data']) return;
//        listItems = recordsResp['data'];
//        autoComplete2.autocomplete(recordsResp['data']);
//    }
//    fetchPageRecords();
//}

let fetchPageRecords = async function (page=1, limit=12) {
    let offset = (page - 1) * limit + 1;
    let offset_records = 1;
    let allRecordsResp = await DbDisplayServices.fetchSelectAll(tableName, limit, offset)
        if (allRecordsResp['status'] == 'error') {
                alert('Error: ' + resp['detail']);
            } else {
                let data = allRecordsResp['data'];
    //            totalRowsMatched.set(allRecordsResp['meta']['total_rows']);
                baseTable.renderRows(data);
            }
            return;
//    baseTable.setLoading(false);
//    if (searchBarInput.value.length == 0) {
//        let allRecordsResp = await DbDisplayServices.fetchSelectAll(tableName, limit, offset)
//        baseTable.setLoading(false);
//        if (allRecordsResp['status'] == 'error') {
//            alert('Error: ' + resp['detail']);
//        } else {
//            let data = allRecordsResp['data'];
////            totalRowsMatched.set(allRecordsResp['meta']['total_rows']);
//            baseTable.renderRows(data);
//        }
//        return;
//    }
//    let response = await DbDisplayServices.fetchSearchWithField(tableName, filteredField, searchBarInput.value, limit, offset);
//    if (!response || response['status'] == 'error') return;
//    let listItems = response['data'];
//    let nRelatedWords = response['meta']['total_rows'];
//    totalRowsMatched.set(nRelatedWords);
//    let recordsResponse = await DbDisplayServices.fetchRecords(tableName, filteredField, listItems, limit, offset_records);
//    baseTable.setLoading(false);
//    if (recordsResponse['meta']['offset'] != offset_records) return;
//    baseTable.renderRows(recordsResponse['data']);
};
//$.fn.fetchPageRecords = fetchPageRecords;
//$.fn.listenClickPageItem = fetchPageRecords;