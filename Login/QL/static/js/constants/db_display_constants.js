const DbDisplayConstants = {
    DEFAULT_PAGE_SIZE: 10,
    DEFAULT_PAGE_NUMBER: 1,
    DEFAULT_LIMIT: 10,
    DEFAULT_OFFSET: 1,
    DEFAULT_AUTOCOMPLETE_LIMIT: 8,
}

export default DbDisplayConstants;