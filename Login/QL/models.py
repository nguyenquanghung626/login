from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    """A typical class defining a model, derived from the Model class."""

    # Fields
    username = models.CharField(max_length=20, help_text='Enter user name')
    password = models.CharField(max_length=20, help_text='Enter password')

    ...

    # Metadata
    class Meta:
        ordering = ['username', 'password']

    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return self.username
