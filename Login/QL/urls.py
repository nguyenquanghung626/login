from . import views
from django.urls import path
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views

urlpatterns = [
    # re_path(r'^index/$', views.index, name='index'),
    path(r'index/', views.index, name="index"),
    path('index/signin/', views.signin, name='signin'),
    path('logout/', views.logout_view, name='logout'),
    path('home/', views.home, name='home'),
    path('data/', views.data, name='data'),
]
